----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:17:58 02/23/2020 
-- Design Name: 
-- Module Name:    LaserDataDynamicDispatcher - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LaserDataDynamicDispatcher is
    Port ( CLK : in  STD_LOGIC;
           DIN : in  STD_LOGIC_VECTOR (47 downto 0);
           WE : in  STD_LOGIC;
           MEM_FULL : out  STD_LOGIC;
           MEM_EMPTY : out  STD_LOGIC;
           DOUT : out  STD_LOGIC_VECTOR (47 downto 0);
           RE : in  STD_LOGIC;
           DR : in  STD_LOGIC);
end LaserDataDynamicDispatcher;

architecture Behavioral of LaserDataDynamicDispatcher is

begin


end Behavioral;

