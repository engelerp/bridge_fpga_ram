----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    10:30:37 02/26/2020
-- Design Name:
-- Module Name:    LaserDataDynamicDispatcher - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SynchronousLaserDataDynamicDispatcher64 is
    Port ( --User Side
			  RESET : in  STD_LOGIC;
			  ERROR : out  STD_LOGIC;
           IDLE : out  STD_LOGIC;
           CLK : in  STD_LOGIC;
           DIN : in  STD_LOGIC_VECTOR (63 downto 0);
           WE : in  STD_LOGIC;
           MEM_FULL : out  STD_LOGIC;
           MEM_EMPTY : out  STD_LOGIC;
           DOUT : out  STD_LOGIC_VECTOR (63 downto 0);
           RE : in  STD_LOGIC;
           RS : in  STD_LOGIC;
           DR : out  STD_LOGIC;
			  MAXADDR_IN : in STD_LOGIC_VECTOR (29 downto 0);

			  --DRAM side
			  --calibration detection
			  calib_done : in STD_LOGIC;
			  --command signals
			  cmd_clk : out STD_LOGIC;
			  cmd_en : out STD_LOGIC;
			  cmd_instr : out STD_LOGIC_VECTOR (2 downto 0);
			  cmd_bl : out STD_LOGIC_VECTOR(5 downto 0);
			  cmd_byte_addr : out STD_LOGIC_VECTOR(29 downto 0);
			  --write signals
			  wr_clk : out STD_LOGIC;
			  wr_en : out STD_LOGIC;
			  wr_mask : out STD_LOGIC_VECTOR(3 downto 0);
			  wr_data : out STD_LOGIC_VECTOR(31 downto 0);
			  --read signals
			  rd_clk : out STD_LOGIC;
			  rd_en : out STD_LOGIC;
			  rd_data : in STD_LOGIC_VECTOR(31 downto 0);
			  rd_empty : in STD_LOGIC;
			  --error signals, we have an error if any of these goes high
			  cmd_full : in STD_LOGIC;
			  wr_full : in STD_LOGIC;
			  wr_underrun : in STD_LOGIC;
			  wr_error : in STD_LOGIC;
			  rd_overflow : in STD_LOGIC;
			  rd_error : in STD_LOGIC;
			  rd_full : in STD_LOGIC
			  );
end SynchronousLaserDataDynamicDispatcher64;

architecture Behavioral of SynchronousLaserDataDynamicDispatcher64 is

--our state type
type state_t is (INIT, IDLE_s, WS_0, WS_1, WS_2, WS_3, WS_4, WS_5, WS_6, WS_7, WS_8, WS_9, WS_10, RS_0, RS_1, RS_2, RS_3, RS_4, RS_5, RS_6, RS_7);

--we need registers for input data
signal iDIN: STD_LOGIC_VECTOR(63 downto 0);
--and also for read data
signal read_data: STD_LOGIC_VECTOR(63 downto 0);
--then we need to detect edges on WE, RE and RS so we need to registers each
signal previous_we, current_we: STD_LOGIC;
signal previous_re, current_re: STD_LOGIC;
signal previous_rs, current_rs: STD_LOGIC;
--obviously, we also need to store the current and the next state
signal current_state: state_t := INIT;
--signal next_state: state_t := INIT;

--we need registers for next read and write addresses
signal write_address: STD_LOGIC_VECTOR(29 downto 0);
signal read_address: STD_LOGIC_VECTOR(29 downto 0);
signal write_increment: STD_LOGIC_VECTOR(29 downto 0);
signal read_increment: STD_LOGIC_VECTOR(29 downto 0);
signal max_address: STD_LOGIC_VECTOR(29 downto 0) := "000100000000000000000000000000";
signal start_address: STD_LOGIC_VECTOR(29 downto 0);

--we may also want to trigger custom errors, so we add another error signal
signal custom_error: STD_LOGIC := '0';

--using a signal, we can also read MEM_FULL and MEM_EMPTY
signal iMEM_FULL, iMEM_EMPTY: STD_LOGIC;

--ERROR : out  STD_LOGIC;
--IDLE : out  STD_LOGIC;
--MEM_FULL : out  STD_LOGIC;
--MEM_EMPTY : out  STD_LOGIC;
--DOUT : out  STD_LOGIC_VECTOR (47 downto 0);
--DR : out  STD_LOGIC;
--command signals
--cmd_clk : out STD_LOGIC;
--cmd_en : out STD_LOGIC;
--cmd_instr : out STD_LOGIC_VECTOR (2 downto 0);
--cmd_bl : out STD_LOGIC_VECTOR(5 downto 0);
--cmd_byte_addr : out STD_LOGIC_VECTOR(29 downto 0);
--write signals
--wr_clk : out STD_LOGIC;
--wr_en : out STD_LOGIC;
--wr_mask : out STD_LOGIC_VECTOR(3 downto 0);
--wr_data : out STD_LOGIC_VECTOR(31 downto 0);
--read signals
--rd_clk : out STD_LOGIC;
--rd_en : out STD_LOGIC;



begin

--fix the constants
--max_address(29 downto 0) <= "001000000000000000000000000000";
--now we write the maximum address from the outside
--max_address(29 downto 0) <= "000100000000000000000000000000"; -- this seems to be the maximum permissible address for our ram
write_increment(29 downto 0) <= "000000000000000000000000001000";
read_increment(29 downto 0) <= "000000000000000000000000001000";
start_address(29 downto 0) <= (others => '0');

--fix ports to internal signals
MEM_FULL <= iMEM_FULL;
MEM_EMPTY <= iMEM_EMPTY;

--clock forwarding
cmd_clk <= CLK;
wr_clk <= CLK;
rd_clk <= CLK;

--port connection
DOUT(63 downto 0) <= read_data(63 downto 0);
ERROR <= cmd_full OR wr_full OR wr_underrun OR wr_error OR rd_overflow OR rd_error OR rd_full OR custom_error;


process(CLK, RESET)
begin
	if(RESET='1') then
		current_state <= INIT;
		--next_state <= INIT;
		--here one should also reset all outputs, else trouble upon use is inbound
		IDLE <= '0';
		iMEM_FULL <= '0';
		iMEM_EMPTY <= '1';
		DR <= '1';
		cmd_en <= '0';
		cmd_instr(2 downto 0) <= (others => '0');
		cmd_bl(5 downto 0) <= (others => '0');
		cmd_byte_addr(29 downto 0) <= (others => '0');
		wr_en <= '0';
		wr_mask(3 downto 0) <= (others => '0');
		wr_data(31 downto 0) <= (others => '0');
		rd_en <= '0';
		current_we <= '0';
		previous_we <= '0';
		current_re <= '0';
		previous_re <= '0';
		current_rs <= '0';
		previous_rs <= '0';
		write_address (29 downto 0) <= (others => '0');
		read_address (29 downto 0) <= (others => '0');
		iDIN(63 downto 0) <= (others => '0');
		read_data(63 downto 0) <= (others => '0');
	elsif rising_edge(CLK) then
		--update state
		--current_state <= next_state;
		--update edge detection signals
		current_we <= WE;
		previous_we <= current_we;
		current_re <= RE;
		previous_re <= current_re;
		current_rs <= RS;
		previous_rs <= current_rs;

		--default register values
		IDLE <= '0';
		DR <= '1';
		cmd_en <= '0';
		cmd_instr(2 downto 0) <= (others => '0');
		cmd_bl(5 downto 0) <= (others => '0');
		cmd_byte_addr(29 downto 0) <= (others => '0');
		wr_en <= '0';
		wr_mask(3 downto 0) <= (others => '0');
		wr_data(31 downto 0) <= (others => '0');
		rd_en <= '0';

		--handle empty and full
		if(write_address >= max_address) then
			iMEM_FULL <= '1';
		else
			iMEM_FULL <= '0';
		end if;
		if(read_address >= write_address or write_address = start_address) then
			iMEM_EMPTY <= '1';
			--we allow changing the max_address when the memory is empty
			max_address <= MAXADDR_IN;
		else
			iMEM_EMPTY <= '0';
		end if;

		case current_state is
			when INIT =>
				write_address <= (others => '0');
				read_address <= (others => '0');
				if(calib_done = '1') then
					--next_state <= IDLE_s;
					current_state <= IDLE_s;
				else
					--next_state <= next_state;
					current_state <= INIT;
				end if;
			when IDLE_s =>
				IDLE <= '1';
				read_address <= (others => '0');
				if(previous_we = '0' and current_we = '1' and iMEM_FULL = '0') then
					--next_state <= WS_0;
					current_state <= WS_0;
				elsif(previous_re = '0' and current_re = '1' and iMEM_EMPTY = '0') then
					--next_state <= RS_0;
					current_state <= RS_0;
				else
					--next_state <= next_state;
					current_state <= IDLE_s;
				end if;
			when WS_0 =>
				--next_state <= WS_1;
				current_state <= WS_1;
				iDIN(63 downto 0) <= DIN(63 downto 0);
			when WS_1 =>
				if(wr_full = '0') then --write data queue has space
					--next_state <= WS_2;
					current_state <= WS_2;
				else --wait for write data queue to make space
					--next_state <= next_state;
					current_state <= WS_1;
				end if;
				wr_data(31 downto 0) <= iDIN(31 downto 0);
			when WS_2 =>
				--next_state <= WS_3;
				current_state <= WS_3;
				wr_en <= '1';
				wr_data(31 downto 0) <= iDIN(31 downto 0);
			when WS_3 =>
				--delay state to bring wr_en down again
				--next_state <= WS_4;
				current_state <= WS_4;
				wr_en <= '0';
				wr_data(31 downto 0) <= iDIN(31 downto 0);
			when WS_4 =>
				if(wr_full = '0') then --write data queue has space
					--next_state <= WS_5;
					current_state <= WS_5;
				else --wait for write data queue to make space
					--next_state <= next_state;
					current_state <= WS_4;
				end if;
				wr_data(31 downto 0) <= iDIN(63 downto 32);
			when WS_5 =>
				--next_state <= WS_6;
				current_state <= WS_6;
				wr_en <= '1';
				wr_data(31 downto 0) <= iDIN(63 downto 32);
			when WS_6 =>
				--delay state to bring wr_en down again
				--next_state <= WS_7;
				current_state <= WS_7;
				wr_en <= '0';
				wr_data(31 downto 0) <= iDIN(63 downto 32);
			when WS_7 =>
				if(cmd_full = '0') then --command queue has space
					--next_state <= WS_8;
					current_state <= WS_8;
				else  --wait for command queue to make space
					--next_state <= next_state;
					current_state <= WS_7;
				end if;
				cmd_instr(2 downto 0) <= "000";
				cmd_bl(5 downto 0) <= "000001";
				cmd_byte_addr(29 downto 0) <= write_address(29 downto 0);
			when WS_8 =>
				--next_state <= WS_9;
				current_state <= WS_9;
				cmd_en <= '1';
				cmd_instr(2 downto 0) <= "000";
				cmd_bl(5 downto 0) <= "000001";
				cmd_byte_addr(29 downto 0) <= write_address(29 downto 0);
			when WS_9 =>
				--next_state <= WS_10;
				current_state <= WS_10;
				cmd_en <= '0';
				--write_address(29 downto 0) <= write_address(29 downto 0) + write_increment(29 downto 0);
				write_address <= write_address + 8;
				cmd_instr(2 downto 0) <= "000";
				cmd_bl(5 downto 0) <= "000001";
				cmd_byte_addr(29 downto 0) <= write_address(29 downto 0);
			when WS_10 =>
				--delay state
				--next_state <= IDLE_s;
        --here we reset the read_data to 0..0
        read_data(63 downto 0) <= (others => '0'); --this was added to fit the bridge
				current_state <= IDLE_s;
			when RS_0 =>
				if(current_rs = '1' and previous_rs = '0') then
					--next_state <= RS_1;
					current_state <= RS_1;
				else
					--next_state <= next_state;
					current_state <= RS_0;
				end if;
			when RS_1 =>
				if(cmd_full = '0') then --command queue has space
					--next_state <= RS_2;
					current_state <= RS_2;
				else --wait for command queue to make space
					--next_state <= next_state;
					current_state <= RS_1;
				end if;
				DR <= '0';
				cmd_instr(2 downto 0) <= "001";
				cmd_bl(5 downto 0) <= "000001";
				cmd_byte_addr(29 downto 0) <= read_address(29 downto 0);
			when RS_2 =>
				--next_state <= RS_3;
				current_state <= RS_3;
				DR <= '0';
				cmd_instr(2 downto 0) <= "001";
				cmd_bl(5 downto 0) <= "000001";
				cmd_byte_addr(29 downto 0) <= read_address(29 downto 0);
				cmd_en <= '1';
			when RS_3 =>
				current_state <= RS_4;
				--read_address(29 downto 0) <= read_address(29 downto 0) + read_increment(29 downto 0);
				read_address <= read_address + 8;
				rd_en <= '1';
				DR <= '0';
				cmd_instr(2 downto 0) <= "001";
				cmd_bl(5 downto 0) <= "000001";
				cmd_byte_addr(29 downto 0) <= read_address(29 downto 0);
				cmd_en <= '0';
			when RS_4 => --catch first packet
				rd_en <= '1';
				DR <= '0';
				if(rd_empty = '0') then
					read_data(31 downto 0) <= rd_data(31 downto 0);
					current_state <= RS_5;
				else
					current_state <= RS_4;
				end if;
			when RS_5 => --catch second packet
				rd_en <= '1';
				DR <= '0';
				if(rd_empty = '0') then
					read_data(63 downto 32) <= rd_data(31 downto 0);
					current_state <= RS_6;
				else
					current_state <= RS_5;
				end if;
			when RS_6 => --delay for safety
				rd_en <= '1';
				DR <= '0';
				current_state <= RS_7;
			when RS_7 =>
				if(iMEM_EMPTY = '1') then
					--next_state <= IDLE_s;
					current_state <= IDLE_s;
					read_address <= (others => '0');
					write_address <= (others => '0');
				else
					--next_state <= RS_0;
					current_state <= RS_0;
				end if;
				DR <= '1';
		end case;
	end if;
end process;

end Behavioral;
