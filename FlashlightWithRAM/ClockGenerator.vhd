library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ClockGenerator is
    Port ( CLK100 : in  STD_LOGIC;
			  Reset: in STD_LOGIC;
           Tick : out  STD_LOGIC);
end ClockGenerator;

architecture Behavioral of ClockGenerator is

signal counter: STD_LOGIC_VECTOR(9 downto 0) := (others => '0');
signal output: STD_LOGIC := '0';

begin

Tick <= output;

updateClock:process(CLK100)
begin
	if rising_edge(CLK100) then
		if Reset = '1' then
			counter <= (others => '0');
			output <= '0';
		else
			if counter = "1011001000" then
				counter <= (others => '0');
				output <= '1';
			else
				counter <= counter + 1;
				output <= '0';
			end if;
		end if;
	end if;
end process;

end Behavioral;

