library IEEE;
use IEEE.STD_LOGIC_1164.all;

package SerialInterface is

type SERIAL_DATA is

record
	Data: std_logic_vector(17 downto 0);
	Command: STD_LOGIC_VECTOR(5 downto 0);
	CommandReceived: STD_LOGIC;
	NX_SINGEN: STD_LOGIC_VECTOR(31 downto 0);
	ADDR_SINE: STD_LOGIC_VECTOR(9 downto 0);
	ADDR_BRAID: STD_LOGIC_VECTOR(14 downto 0);
	Enabled: STD_LOGIC;
end record;


end SerialInterface;

package body SerialInterface is
 
end SerialInterface;
