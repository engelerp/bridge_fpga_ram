library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ReceivingEngine is
    Port ( CLK100 : in  STD_LOGIC;
           SIGNAL_CLOCK : in  STD_LOGIC;
           DATA : in  STD_LOGIC;
           DISTANCE : out  STD_LOGIC_VECTOR (47 downto 0);
           DATA_READY : out  STD_LOGIC);
end ReceivingEngine;

architecture Behavioral of ReceivingEngine is

signal clockCounter: STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
signal shiftReg: STD_LOGIC_VECTOR(48 downto 0) := (others => '0');
signal outputData: STD_LOGIC_VECTOR(47 downto 0) := (others => '0');

signal buffCLK: STD_LOGIC_VECTOR(2 downto 0);
signal buffSIG: STD_LOGIC_VECTOR(2 downto 0);

signal previousCLK: STD_LOGIC := '0';
signal DR: STD_LOGIC := '0';
signal outR: STD_LOGIC := '0';

begin

DISTANCE <= outputData;
DATA_READY <= outR;

updateOut: process(CLK100)
begin
	if rising_edge(CLK100) then
		if DR = '1' then
			outputData <= shiftReg(47 downto 0);
			outR <= '1';
		else
			outputData <= outputData;
			outR <= '0';
		end if;
	end if;
end process;


updatePrevCLK: process(CLK100)
begin
	if rising_edge(CLK100) then
		previousCLK <= buffCLK(0);
	end if;
end process;

receveData: process(CLK100)
begin
	if rising_edge(CLK100) then
		if buffCLK(0) = '1' then
			clockCounter <= (others => '0');
			shiftReg <= shiftReg;
			DR <= '0';
		else
			if clockCounter = "1000" then
				shiftReg(0) <= '1';
				shiftReg(48 downto 1) <= (others => '0');
				clockCounter <= clockCounter;
				DR <= '0';
			else
				clockCounter <= clockCounter + 1;
				if ((shiftReg(48) = '0') AND (previousCLK = '1') AND (buffCLK(0) = '0')) then
					shiftReg(0) <= buffSIG(0);
					shiftReg(48 downto 1) <= shiftReg(47 downto 0);
					DR <= shiftReg(47);
				else 
					shiftReg <= shiftReg;
					DR <= '0';
				end if;
			end if;
		end if;
	end if;
end process;

sanitizeClock: process(CLK100)
begin
	if rising_edge(CLK100) then
		buffCLK(2) <= SIGNAL_CLOCK;
		buffCLK(1 downto 0) <= buffCLK(2 downto 1);
	end if;
end process;

sanitizeSignal: process(CLK100)
begin
	if rising_edge(CLK100) then
		buffSIG(2) <= DATA;
		buffSIG(1 downto 0) <= buffSIG(2 downto 1);
	end if;
end process;

end Behavioral;

