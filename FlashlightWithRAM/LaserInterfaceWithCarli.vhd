library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.SerialInterface.ALL;

entity LaserInterfaceWithCarli is
    Port ( c3_sys_clk : in  STD_LOGIC; --direct clock input, to carli64/c3_sys_clk
           LS1 : in  STD_LOGIC;
           LC1 : in  STD_LOGIC;
           LS2 : in  STD_LOGIC;
           LC2 : in  STD_LOGIC;
           LS3 : in  STD_LOGIC;
           LC3 : in  STD_LOGIC;
           sampleClock : in  STD_LOGIC;
			     RX_SR: in STD_LOGIC;
           TX : out  STD_LOGIC;
			     CTS: out STD_LOGIC;

           CLKPLL100 : out STD_LOGIC; --pll output, to be used in rest of design
           ERROR : out STD_LOGIC; --error signal
           mcb3_dram_dq : inout  STD_LOGIC_VECTOR (15 downto 0); --LPDDR pin
           mcb3_dram_a : out  STD_LOGIC_VECTOR (12 downto 0); --LPDDR pin
           mcb3_dram_ba : out  STD_LOGIC_VECTOR (1 downto 0); --LPDDR pin
           mcb3_dram_ras_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_cas_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_we_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_cke : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_ck : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_ck_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_dqs : inout  STD_LOGIC; --LPDDR pin
           mcb3_dram_udqs : inout  STD_LOGIC; --LPDDR pin
           mcb3_dram_udm : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_dm : out  STD_LOGIC; --LPDDR pin
           mcb3_rzq : inout  STD_LOGIC; --LPDDR pin
           c3_sys_rst_n : in  STD_LOGIC);
end LaserInterfaceWithCarli;

architecture Behavioral of LaserInterfaceWithCarli is

--carli signals
signal CLK100: STD_LOGIC;
signal c_clk_out: STD_LOGIC;
signal c_idle: STD_LOGIC;
signal c_clk_out: STD_LOGIC;
signal c_din: STD_LOGIC_VECTOR(63 downto 0) := (others => '0');
signal c_we: STD_LOGIC := '0';
signal c_mem_full: STD_LOGIC;
signal c_dout: STD_LOGIC_VECTOR(63 downto 0);
signal c_re: STD_LOGIC := '0';
signal c_rs: STD_LOGIC := '0';
signal c_dr: STD_LOGIC;
signal c_maxaddr: STD_LOGIC_VECTOR(29 downto 0) := "000000000001000000000000000000";

signal maxRecordingPointer: STD_LOGIC_VECTOR(29 downto 0) := "000000000000001000000000000000";



signal dataToSendTF: STD_LOGIC_VECTOR(47 downto 0) := (others  => '0');
signal sendCommandTF: STD_LOGIC := '0';
signal sendCommandTF_Delay1: STD_LOGIC := '0';
signal sendCommandTF_Delay2: STD_LOGIC := '0';
signal selectWordDelayed: STD_LOGIC := '0';

signal dataToSend: STD_LOGIC_VECTOR(47 downto 0) := (others  => '0');
signal sendCommand: STD_LOGIC := '0';

signal distanceAxis1: STD_LOGIC_VECTOR(47 downto 0);
signal distanceAxis2: STD_LOGIC_VECTOR(47 downto 0);
signal distanceAxis3: STD_LOGIC_VECTOR(47 downto 0);

signal dataReady1: STD_LOGIC;
signal dataReady2: STD_LOGIC;
signal dataReady3: STD_LOGIC;
signal serialData: SERIAL_DATA;

-- Variables for trace acquisition
signal selectedAxis: STD_LOGIC_VECTOR(1 downto 0) := (others  => '0');
signal selectedData: STD_LOGIC_VECTOR(47 downto 0) := (others  => '0');
signal selectedDataReady: STD_LOGIC := '0';

signal recordingScheduled: STD_LOGIC := '0';
signal recordingStarted: STD_LOGIC := '0';
signal recordingStartPoint: STD_LOGIC_VECTOR(29 downto 0) := (others  => '0');
signal recordingStartCounter: STD_LOGIC_VECTOR(29 downto 0) := (others  => '0');
signal recordingLastSampleDelay: STD_LOGIC_VECTOR(14 downto 0) := (others  => '0');
signal recordingPointer: STD_LOGIC_VECTOR(15 downto 0) := (others  => '0');

signal transferSampleCounter: STD_LOGIC_VECTOR(16 downto 0) := (others => '0');
signal transferTimeCounter: STD_LOGIC_VECTOR(10 downto 0) := std_logic_vector(to_unsigned(1615,11));

signal memoryWriteWord: STD_LOGIC_VECTOR(53 downto 0) := (others => '0');
signal memoryWriteEnable: STD_LOGIC_VECTOR(0 downto 0) := "0";
signal memoryWriteAddress: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');

signal memoryReadWord: STD_LOGIC_VECTOR(53 downto 0);
signal memoryReadAddress: STD_LOGIC_VECTOR(15 downto 0);

signal clk100SinceMeasurementEnded: STD_LOGIC_VECTOR(31 downto 0) := (others  => '0');

signal transferEndIndex: STD_LOGIC_VECTOR(16 downto 0) := (others => '0');


component ReceivingEngine is
    Port ( CLK100 : in  STD_LOGIC;
           SIGNAL_CLOCK : in  STD_LOGIC;
           DATA : in  STD_LOGIC;
           DISTANCE : out  STD_LOGIC_VECTOR (47 downto 0);
           DATA_READY : out  STD_LOGIC);
end component;

component SerialSend is
    Port ( CLK100 : in  STD_LOGIC;
           Data : in  STD_LOGIC_VECTOR (47 downto 0);
           Transfer : in  STD_LOGIC;
           TX : out  STD_LOGIC);
end component;

component SerialController is
    Port ( CLK100: in  STD_LOGIC;
           RX: in  STD_LOGIC;
			  BoardID: in STD_LOGIC_VECTOR(3 downto 0);
           SerialData: out  SERIAL_DATA);
end component;

COMPONENT TraceMemory
  PORT (
    clka : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(53 DOWNTO 0);
    clkb : IN STD_LOGIC;
    addrb : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(53 DOWNTO 0)
  );
END COMPONENT;

COMPONENT carli64
    PORT ( RESET : in  STD_LOGIC; --holds the controller in reset
           ERROR : out  STD_LOGIC; --goes high when any error in the chain is detected
           IDLE : out  STD_LOGIC; --indicates the controller is idling
           CLK_OUT : out  STD_LOGIC; --100 MHz PLL output clock
           DIN : in  STD_LOGIC_VECTOR (63 downto 0); --data to write
           WE : in  STD_LOGIC; --write enable
           MEM_FULL : out  STD_LOGIC; --high when memory is full
           MEM_EMPTY : out  STD_LOGIC; --high when memory is empty
           DOUT : out  STD_LOGIC_VECTOR (63 downto 0); --data that was read
           RE : in  STD_LOGIC; --read enable
           RS : in  STD_LOGIC; --read strobe
           DR : out  STD_LOGIC; --data ready
           MAXADDR_IN : in  STD_LOGIC_VECTOR (29 downto 0); --maximum permissible address. latched when memory is empty
           mcb3_dram_dq : inout  STD_LOGIC_VECTOR (15 downto 0); --LPDDR pin
           mcb3_dram_a : out  STD_LOGIC_VECTOR (12 downto 0); --LPDDR pin
           mcb3_dram_ba : out  STD_LOGIC_VECTOR (1 downto 0); --LPDDR pin
           mcb3_dram_ras_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_cas_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_we_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_cke : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_ck : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_ck_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_dqs : inout  STD_LOGIC; --LPDDR pin
           mcb3_dram_udqs : inout  STD_LOGIC; --LPDDR pin
           mcb3_dram_udm : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_dm : out  STD_LOGIC; --LPDDR pin
           mcb3_rzq : inout  STD_LOGIC; --LPDDR pin
           c3_sys_clk : in STD_LOGIC; --100 MHz clock input
           c3_sys_rst_n : in  STD_LOGIC);
END COMPONENT;

begin

iSerialReceive: SerialController port map(CLK100, RX_SR, "1110", SerialData);

iReceivingEngine1: ReceivingEngine port map(CLK100, LC1, LS1, distanceAxis1, dataReady1);
iReceivingEngine2: ReceivingEngine port map(CLK100, LC2, LS2, distanceAxis2, dataReady2);
iReceivingEngine3: ReceivingEngine port map(CLK100, LC3, LS3, distanceAxis3, dataReady3);

iSerialSend: SerialSend port map(CLK100, dataToSend, sendCommand, TX);

--Ports: CLKA, WEA, ADDRA, DINA, CLKB, ADDRB, DOUTB
iTraceMemory: TraceMemory port map(CLK100, memoryWriteEnable, memoryWriteAddress(14 downto 0), memoryWriteWord, CLK100, memoryReadAddress(14 downto 0), memoryReadWord);

iCarli: carli64 port map(
    RESET => '0',
    ERROR => ERROR,
    IDLE => c_idle,
    CLK_OUT => c_clk_out,
    DIN => c_din,
    WE => c_we,
    MEM_FULL => c_mem_full,
    DOUT => c_dout,
    RE => c_re,
    RS => c_rs,
    DR => c_dr,
    MAXADDR_IN => c_maxaddr,
    mcb3_dram_dq => mcb3_dram_dq,
    mcb3_dram_a => mcb3_dram_a,
    mcb3_dram_ba => mcb3_dram_ba,
    mcb3_dram_ras_n => mcb3_dram_ras_n,
    mcb3_dram_cas_n => mcb3_dram_cas_n,
    mcb3_dram_we_n => mcb3_dram_we_n,
    mcb3_dram_cke => mcb3_dram_cke,
    mcb3_dram_ck => mcb3_dram_ck,
    mcb3_dram_ck_n => mcb3_dram_ck_n,
    mcb3_dram_dqs => mcb3_dram_dqs,
    mcb3_dram_udqs => mcb3_dram_udqs,
    mcb3_dram_udm => mcb3_dram_udm,
    mcb3_dram_dm => macb3_dram_dm,
    mcb3_rzq => mcb3_rzq,
    c3_sys_clk => c3_sys_clk,
    c3_sys_rst_n => c3_sys_rst_n
);

--connect carli signals
CLKPLL100 <= c_clk_out;
CLK100 <= c_clk_out;

c_we <= memoryWriteEnable;
c_din(53 downto 0) <= memoryWriteWord(53 downto 0);
c_din(63 downto 54) <= (others => '0');
memoryReadWord(53 downto 0) <= c_dout(53 downto 0);



CTS <= SerialData.Enabled;

updateCLK100SinceLastMeasurement: process(CLK100)
begin
	if rising_edge(CLK100) then
		if recordingPointer = 32768 then
			clk100SinceMeasurementEnded <= clk100SinceMeasurementEnded + 1;
		else
			clk100SinceMeasurementEnded <= (others => '0');
		end if;
	end if;
end process;

writeToTransferEngine: process(CLK100)
begin
	if rising_edge(CLK100) then
		sendCommandTF <= sendCommandTF_Delay1;
		sendCommandTF_Delay1 <= sendCommandTF_Delay2;
		selectWordDelayed <= transferSampleCounter(0);
		--dataToSendTF(15 downto 0) <= recordingPointer;
		if selectWordDelayed = '1' then
			dataToSendTF(39 downto 0)  <= memoryReadWord(39 downto 0);
		  	dataToSendTF(47 downto 40) <= (others => '0');
		  else
			dataToSendTF(13 downto 0)  <= memoryReadWord(53 downto 40);
			dataToSendTF(47 downto 14) <= (others => '0');
		end if;
	end if;
end process;

sequenceTransfer: process(CLK100)
begin
	if rising_edge(CLK100) then
		if ((SerialData.CommandReceived = '1') AND (SerialData.Command = std_logic_vector(to_unsigned(61,6)))) then
			transferEndIndex <= SerialData.NX_SINGEN(16 downto 0);
			if SerialData.NX_SINGEN(31) = '1' then --probably used to set only the transferEndIndex without starting a transfer
				transferSampleCounter <= SerialData.NX_SINGEN(16 downto 0);
			else
				transferSampleCounter <= transferSampleCounter;
			end if;
			sendCommandTF_Delay2 <= '0';
			transferTimeCounter <= transferTimeCounter;
			memoryReadAddress <= memoryReadAddress;
		elsif ((SerialData.CommandReceived = '1') AND (SerialData.Command = std_logic_vector(to_unsigned(58,6)))) then --set starting point of transfer
			transferSampleCounter <= SerialData.NX_SINGEN(16 downto 0);
			transferTimeCounter <= (others => '0');
			sendCommandTF_Delay2 <= '0';
			memoryReadAddress <= memoryReadAddress;
			transferEndIndex <= transferEndIndex;
		else
			transferEndIndex <= transferEndIndex;
			if transferTimeCounter = std_logic_vector(to_unsigned(1950,11)) then
				transferTimeCounter <= (others => '0');
				if transferSampleCounter = transferEndIndex then
					sendCommandTF_Delay2 <= '0';
					transferSampleCounter <= transferSampleCounter;
					memoryReadAddress <= memoryReadAddress;
				else
					transferSampleCounter <= transferSampleCounter + 1;
					memoryReadAddress <= transferSampleCounter(16 downto 1);
					sendCommandTF_Delay2 <= '1';
				end if;
			else
				transferTimeCounter <= transferTimeCounter + 1;
				sendCommandTF_Delay2 <= '0';
				transferSampleCounter <= transferSampleCounter;
				memoryReadAddress <= memoryReadAddress;
			end if;
		end if;
	end if;
end process;

processSchedule: process(CLK100)
begin
	if rising_edge(CLK100) then
		if ((SerialData.CommandReceived = '1') AND (SerialData.Command = std_logic_vector(to_unsigned(57,6)))) then
			recordingScheduled <= '1';
			recordingStarted <= '0';
			recordingStartCounter <= (others => '0');
			recordingStartPoint <= SerialData.NX_SINGEN(29 downto 0);
			selectedAxis <= SerialData.NX_SINGEN(31 downto 30);
		elsif (recordingScheduled = '1') AND (sampleClock = '1') then
			selectedAxis <= selectedAxis;
			recordingStartPoint <= recordingStartPoint;
			if recordingStartCounter = recordingStartPoint then
				recordingStartCounter <= recordingStartCounter;
				recordingStarted <= '1';
				recordingScheduled <= '0';
			else
				recordingStartCounter <= recordingStartCounter + 1;
				recordingStarted <= '0';
				recordingScheduled <= '1';
			end if;
		else
			selectedAxis <= selectedAxis;
			recordingStartPoint <= recordingStartPoint;
			recordingStartCounter <= recordingStartCounter;
			recordingStarted <= recordingStarted;
			recordingScheduled <= recordingScheduled;
		end if;
	end if;
end process;

performRecording: process(CLK100)
begin
	if rising_edge(CLK100) then
		if recordingStarted = '0' then
			memoryWriteWord <= memoryWriteWord;
			memoryWriteEnable <= "0";
			memoryWriteAddress <= memoryWriteAddress;
			recordingLastSampleDelay <= (others => '0');
			recordingPointer <= (others => '0');
		else
			if selectedDataReady = '1' then
				if recordingPointer = 32768 then
					memoryWriteWord <= memoryWriteWord;
					memoryWriteEnable <= "0";
					memoryWriteAddress <= memoryWriteAddress;
					recordingLastSampleDelay <= (others => '0');
					recordingPointer <= recordingPointer;
				else
					memoryWriteWord(39 downto 0) <= selectedData(39 downto 0);
					memoryWriteWord(53 downto 40) <= recordingLastSampleDelay(13 downto 0);
					memoryWriteEnable <= "1";
					memoryWriteAddress <= recordingPointer;
					recordingLastSampleDelay(14 downto 1) <= (others => '0');
					recordingLastSampleDelay(0) <= '1';
					recordingPointer <= recordingPointer + 1;
				end if;
			else
				memoryWriteWord <= memoryWriteWord;
				memoryWriteEnable <= "0";
				memoryWriteAddress <= recordingPointer;
				recordingLastSampleDelay <= recordingLastSampleDelay + 1;
				recordingPointer <= recordingPointer;
			end if;
		end if;
	end if;
end process;

updateSelectedData: process(CLK100)
begin
	if rising_edge(CLK100) then
		if selectedAxis = "00" then
			selectedData <= distanceAxis1;
			selectedDataReady <= dataReady1;
		elsif selectedAxis = "01" then
			selectedData <= distanceAxis2;
			selectedDataReady <= dataReady2;
		elsif selectedAxis = "10" then
			selectedData <= distanceAxis3;
			selectedDataReady <= dataReady3;
		else
			selectedData <= (others => '0');
			selectedDataReady <= '0';
		end if;
	end if;
end process;

respondToCommand: process(CLK100)
begin
	if rising_edge(CLK100) then
		if ((SerialData.CommandReceived = '1') AND (SerialData.Command = std_logic_vector(to_unsigned(54,6)))) then
			dataToSend <= distanceAxis1;
			sendCommand <= '1';
		elsif ((SerialData.CommandReceived = '1') AND (SerialData.Command = std_logic_vector(to_unsigned(55,6)))) then
			dataToSend <= distanceAxis2;
			sendCommand <= '1';
		elsif ((SerialData.CommandReceived = '1') AND (SerialData.Command = std_logic_vector(to_unsigned(56,6)))) then
			dataToSend <= distanceAxis3;
			sendCommand <= '1';
		elsif ((SerialData.CommandReceived = '1') AND (SerialData.Command = std_logic_vector(to_unsigned(59,6)))) then
			dataToSend(31 downto 0) <= clk100SinceMeasurementEnded;
			dataToSend(47 downto 32) <= (others => '0');
			sendCommand <= '1';
		elsif ((SerialData.CommandReceived = '1') AND (SerialData.Command = std_logic_vector(to_unsigned(60,6)))) then --are we there yet command
			dataToSend(15 downto 0) <= recordingPointer;
			dataToSend(16) <= '0';
			dataToSend(17) <= recordingScheduled;
			dataToSend(47 downto 18) <= (others => '0');
			sendCommand <= '1';
		else
			dataToSend <= dataToSendTF;
			sendCommand <= sendCommandTF;
		end if;
	end if;
end process;

end Behavioral;
