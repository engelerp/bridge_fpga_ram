----------------------------------------------------------------------------------
-- Company: ETH Zurich
-- Engineer: Pascal Engeler
--
-- Create Date:    15:24:27 03/17/2020
-- Design Name: 	 Controller Amenity for Rbcomb-Lpddr Interfacing (carli)
-- Module Name:    carli - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--Controller Amenity for Rbcomb Lpddr Interfacing
entity carli64 is
    Port ( RESET : in  STD_LOGIC; --holds the controller in reset
           ERROR : out  STD_LOGIC; --goes high when any error in the chain is detected
           IDLE : out  STD_LOGIC; --indicates the controller is idling
           CLK_OUT : out  STD_LOGIC; --100 MHz PLL output clock
           DIN : in  STD_LOGIC_VECTOR (63 downto 0); --data to write
           WE : in  STD_LOGIC; --write enable
           MEM_FULL : out  STD_LOGIC; --high when memory is full
           MEM_EMPTY : out  STD_LOGIC; --high when memory is empty
           DOUT : out  STD_LOGIC_VECTOR (63 downto 0); --data that was read
           RE : in  STD_LOGIC; --read enable
           RS : in  STD_LOGIC; --read strobe
           DR : out  STD_LOGIC; --data ready
           MAXADDR_IN : in  STD_LOGIC_VECTOR (29 downto 0); --maximum permissible address. latched when memory is empty
           mcb3_dram_dq : inout  STD_LOGIC_VECTOR (15 downto 0); --LPDDR pin
           mcb3_dram_a : out  STD_LOGIC_VECTOR (12 downto 0); --LPDDR pin
           mcb3_dram_ba : out  STD_LOGIC_VECTOR (1 downto 0); --LPDDR pin
           mcb3_dram_ras_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_cas_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_we_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_cke : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_ck : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_ck_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_dqs : inout  STD_LOGIC; --LPDDR pin
           mcb3_dram_udqs : inout  STD_LOGIC; --LPDDR pin
           mcb3_dram_udm : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_dm : out  STD_LOGIC; --LPDDR pin
           mcb3_rzq : inout  STD_LOGIC; --LPDDR pin
           c3_sys_clk : in STD_LOGIC; --100 MHz clock input
           c3_sys_rst_n : in  STD_LOGIC);
end carli64;

architecture Behavioral of carli64 is


--components
component SynchronousLaserDataDynamicDispatcher64 is
    Port ( --User Side
			  RESET : in  STD_LOGIC;
			  ERROR : out  STD_LOGIC;
           IDLE : out  STD_LOGIC;
           CLK : in  STD_LOGIC;
           DIN : in  STD_LOGIC_VECTOR (63 downto 0);
           WE : in  STD_LOGIC;
           MEM_FULL : out  STD_LOGIC;
           MEM_EMPTY : out  STD_LOGIC;
           DOUT : out  STD_LOGIC_VECTOR (63 downto 0);
           RE : in  STD_LOGIC;
           RS : in  STD_LOGIC;
           DR : out  STD_LOGIC;
			  MAXADDR_IN : in STD_LOGIC_VECTOR (29 downto 0);

			  --DRAM side
			  --calibration detection
			  calib_done : in STD_LOGIC;
			  --command signals
			  cmd_clk : out STD_LOGIC;
			  cmd_en : out STD_LOGIC;
			  cmd_instr : out STD_LOGIC_VECTOR (2 downto 0);
			  cmd_bl : out STD_LOGIC_VECTOR(5 downto 0);
			  cmd_byte_addr : out STD_LOGIC_VECTOR(29 downto 0);
			  --write signals
			  wr_clk : out STD_LOGIC;
			  wr_en : out STD_LOGIC;
			  wr_mask : out STD_LOGIC_VECTOR(3 downto 0);
			  wr_data : out STD_LOGIC_VECTOR(31 downto 0);
			  --read signals
			  rd_clk : out STD_LOGIC;
			  rd_en : out STD_LOGIC;
			  rd_data : in STD_LOGIC_VECTOR(31 downto 0);
			  rd_empty : in STD_LOGIC;
			  --error signals, we have an error if any of these goes high
			  cmd_full : in STD_LOGIC;
			  wr_full : in STD_LOGIC;
			  wr_underrun : in STD_LOGIC;
			  wr_error : in STD_LOGIC;
			  rd_overflow : in STD_LOGIC;
			  rd_error : in STD_LOGIC;
			  rd_full : in STD_LOGIC
			  );
end component;

component s6_lpddr
 generic(
    C3_P0_MASK_SIZE           : integer := 4;
    C3_P0_DATA_PORT_SIZE      : integer := 32;
    C3_P1_MASK_SIZE           : integer := 4;
    C3_P1_DATA_PORT_SIZE      : integer := 32;
    C3_MEMCLK_PERIOD          : integer := 10000;
    C3_RST_ACT_LOW            : integer := 0;
    C3_INPUT_CLK_TYPE         : string := "SINGLE_ENDED";
    C3_CALIB_SOFT_IP          : string := "TRUE";
    C3_SIMULATION             : string := "FALSE";
    DEBUG_EN                  : integer := 0;
    C3_MEM_ADDR_ORDER         : string := "ROW_BANK_COLUMN";
    C3_NUM_DQ_PINS            : integer := 16;
    C3_MEM_ADDR_WIDTH         : integer := 13;
    C3_MEM_BANKADDR_WIDTH     : integer := 2
);
    port (
   mcb3_dram_dq                            : inout  std_logic_vector(C3_NUM_DQ_PINS-1 downto 0);
   mcb3_dram_a                             : out std_logic_vector(C3_MEM_ADDR_WIDTH-1 downto 0);
   mcb3_dram_ba                            : out std_logic_vector(C3_MEM_BANKADDR_WIDTH-1 downto 0);
   mcb3_dram_cke                           : out std_logic;
   mcb3_dram_ras_n                         : out std_logic;
   mcb3_dram_cas_n                         : out std_logic;
   mcb3_dram_we_n                          : out std_logic;
   mcb3_dram_dm                            : out std_logic;
   mcb3_dram_udqs                          : inout  std_logic;
   mcb3_rzq                                : inout  std_logic;
   mcb3_dram_udm                           : out std_logic;
   c3_sys_clk                              : in  std_logic;
   c3_sys_rst_n                            : in  std_logic;
   c3_calib_done                           : out std_logic;
   c3_clk0                                 : out std_logic;
   c3_rst0                                 : out std_logic;
	c3_clk4											 : out std_logic;
   mcb3_dram_dqs                           : inout  std_logic;
   mcb3_dram_ck                            : out std_logic;
   mcb3_dram_ck_n                          : out std_logic;
   c3_p0_cmd_clk                           : in std_logic;
   c3_p0_cmd_en                            : in std_logic;
   c3_p0_cmd_instr                         : in std_logic_vector(2 downto 0);
   c3_p0_cmd_bl                            : in std_logic_vector(5 downto 0);
   c3_p0_cmd_byte_addr                     : in std_logic_vector(29 downto 0);
   c3_p0_cmd_empty                         : out std_logic;
   c3_p0_cmd_full                          : out std_logic;
   c3_p0_wr_clk                            : in std_logic;
   c3_p0_wr_en                             : in std_logic;
   c3_p0_wr_mask                           : in std_logic_vector(C3_P0_MASK_SIZE - 1 downto 0);
   c3_p0_wr_data                           : in std_logic_vector(C3_P0_DATA_PORT_SIZE - 1 downto 0);
   c3_p0_wr_full                           : out std_logic;
   c3_p0_wr_empty                          : out std_logic;
   c3_p0_wr_count                          : out std_logic_vector(6 downto 0);
   c3_p0_wr_underrun                       : out std_logic;
   c3_p0_wr_error                          : out std_logic;
   c3_p0_rd_clk                            : in std_logic;
   c3_p0_rd_en                             : in std_logic;
   c3_p0_rd_data                           : out std_logic_vector(C3_P0_DATA_PORT_SIZE - 1 downto 0);
   c3_p0_rd_full                           : out std_logic;
   c3_p0_rd_empty                          : out std_logic;
   c3_p0_rd_count                          : out std_logic_vector(6 downto 0);
   c3_p0_rd_overflow                       : out std_logic;
   c3_p0_rd_error                          : out std_logic
);
end component;


-- SLDDD signals
signal sld3_clk : STD_LOGIC;

signal NET_c3_calib_done : STD_LOGIC;
signal NET_c3_p0_cmd_clk : STD_LOGIC;
signal NET_c3_p0_cmd_en : STD_LOGIC;
signal NET_c3_p0_cmd_instr : STD_LOGIC_VECTOR (2 downto 0);
signal NET_c3_p0_cmd_bl : STD_LOGIC_VECTOR (5 downto 0);
signal NET_c3_p0_cmd_byte_addr : STD_LOGIC_VECTOR (29 downto 0);
signal NET_c3_p0_wr_clk : STD_LOGIC;
signal NET_c3_p0_wr_en : STD_LOGIC;
signal NET_c3_p0_wr_mask : STD_LOGIC_VECTOR (3 downto 0);
signal NET_c3_p0_wr_data : STD_LOGIC_VECTOR (31 downto 0);
signal NET_c3_p0_rd_clk : STD_LOGIC;
signal NET_c3_p0_rd_en : STD_LOGIC;
signal NET_c3_p0_rd_data : STD_LOGIC_VECTOR (31 downto 0);
signal NET_c3_p0_rd_empty : STD_LOGIC;
signal NET_c3_p0_cmd_full : STD_LOGIC;
signal NET_c3_p0_wr_full : STD_LOGIC;
signal NET_c3_p0_wr_underrun : STD_LOGIC;
signal NET_c3_p0_wr_error : STD_LOGIC;
signal NET_c3_p0_rd_overflow : STD_LOGIC;
signal NET_c3_p0_rd_error : STD_LOGIC;
signal NET_c3_p0_rd_full : STD_LOGIC;

--unconnected signals
signal NET_c3_p0_cmd_empty : STD_LOGIC;
signal NET_c3_p0_wr_empty : STD_LOGIC;
signal NET_c3_p0_wr_count : STD_LOGIC_VECTOR (6 downto 0);
signal NET_c3_p0_rd_count : STD_LOGIC_VECTOR (6 downto 0);

--clocks
signal c3_clk4 : STD_LOGIC;
signal c3_clk0 : STD_LOGIC;

--reset
signal c3_rst0 : STD_LOGIC;





begin

--connect clock output
CLK_OUT <= c3_clk4;


u_s6_lpddr : s6_lpddr
  generic map (
  C3_P0_MASK_SIZE 		=> 	4,
  C3_P0_DATA_PORT_SIZE 	=> 	32,
  C3_P1_MASK_SIZE 		=> 	4,
  C3_P1_DATA_PORT_SIZE 	=> 	32,
  C3_MEMCLK_PERIOD 		=> 	10000,
  C3_RST_ACT_LOW 			=> 	0,
  C3_INPUT_CLK_TYPE 		=> 	"SINGLE_ENDED",
  C3_CALIB_SOFT_IP 		=> 	"TRUE",
  C3_SIMULATION 			=> 	"FALSE",
  DEBUG_EN 					=> 	0,
  C3_MEM_ADDR_ORDER 		=> 	"ROW_BANK_COLUMN",
  C3_NUM_DQ_PINS 			=> 	16,
  C3_MEM_ADDR_WIDTH 		=> 	13,
  C3_MEM_BANKADDR_WIDTH => 	2)
  port map (

  c3_sys_clk  				=>    c3_sys_clk,
  c3_sys_rst_n   			=>    c3_sys_rst_n,

  mcb3_dram_dq       	=>    mcb3_dram_dq,
  mcb3_dram_a        	=>    mcb3_dram_a,
  mcb3_dram_ba       	=>    mcb3_dram_ba,
  mcb3_dram_ras_n    	=>    mcb3_dram_ras_n,
  mcb3_dram_cas_n    	=>    mcb3_dram_cas_n,
  mcb3_dram_we_n     	=>    mcb3_dram_we_n,
  mcb3_dram_cke      	=>    mcb3_dram_cke,
  mcb3_dram_ck       	=>    mcb3_dram_ck,
  mcb3_dram_ck_n     	=>    mcb3_dram_ck_n,
  mcb3_dram_dqs      	=>    mcb3_dram_dqs,
  mcb3_dram_udqs  		=>    mcb3_dram_udqs,    -- for X16 parts
  mcb3_dram_udm  			=>    mcb3_dram_udm,     -- for X16 parts
  mcb3_dram_dm  			=>    mcb3_dram_dm,

  c3_clk0					=>	   c3_clk0,
  c3_rst0					=>    c3_rst0,
  c3_clk4 					=> 	c3_clk4, --Added by Pascal Engeler

  c3_calib_done      	=>    NET_c3_calib_done,

  mcb3_rzq         		=>    mcb3_rzq,

  c3_p0_cmd_clk      	=>  	NET_c3_p0_cmd_clk,
  c3_p0_cmd_en          =>  	NET_c3_p0_cmd_en,
  c3_p0_cmd_instr       =>  	NET_c3_p0_cmd_instr,
  c3_p0_cmd_bl          =>  	NET_c3_p0_cmd_bl,
  c3_p0_cmd_byte_addr   =>  	NET_c3_p0_cmd_byte_addr,
  c3_p0_cmd_empty       =>  	NET_c3_p0_cmd_empty,
  c3_p0_cmd_full        =>  	NET_c3_p0_cmd_full,
  c3_p0_wr_clk          =>  	NET_c3_p0_wr_clk,
  c3_p0_wr_en           =>  	NET_c3_p0_wr_en,
  c3_p0_wr_mask         =>  	NET_c3_p0_wr_mask,
  c3_p0_wr_data         =>  	NET_c3_p0_wr_data,
  c3_p0_wr_full         =>  	NET_c3_p0_wr_full,
  c3_p0_wr_empty        =>  	NET_c3_p0_wr_empty,
  c3_p0_wr_count        =>  	NET_c3_p0_wr_count,
  c3_p0_wr_underrun     =>  	NET_c3_p0_wr_underrun,
  c3_p0_wr_error        =>  	NET_c3_p0_wr_error,
  c3_p0_rd_clk          =>  	NET_c3_p0_rd_clk,
  c3_p0_rd_en           =>  	NET_c3_p0_rd_en,
  c3_p0_rd_data         =>  	NET_c3_p0_rd_data,
  c3_p0_rd_full         =>  	NET_c3_p0_rd_full,
  c3_p0_rd_empty        =>  	NET_c3_p0_rd_empty,
  c3_p0_rd_count        =>  	NET_c3_p0_rd_count,
  c3_p0_rd_overflow     =>  	NET_c3_p0_rd_overflow,
  c3_p0_rd_error        =>  	NET_c3_p0_rd_error
);



LDDD : SynchronousLaserDataDynamicDispatcher
  port map (
  RESET 						=> 	RESET,
  ERROR 						=> 	ERROR,
  IDLE 						=> 	IDLE,
  CLK 						=> 	c3_clk4,
  DIN 						=> 	DIN,
  WE 							=> 	WE,
  MEM_FULL 					=> 	MEM_FULL,
  MEM_EMPTY			 		=> 	MEM_EMPTY,
  DOUT 						=> 	DOUT,
  RE 							=> 	RE,
  RS 							=> 	RS,
  DR 							=> 	DR,
  MAXADDR_IN 				=> 	MAXADDR_IN,

  calib_done 				=> 	NET_c3_calib_done,
  cmd_clk 					=> 	NET_c3_p0_cmd_clk,
  cmd_en 					=> 	NET_c3_p0_cmd_en,
  cmd_instr 				=> 	NET_c3_p0_cmd_instr,
  cmd_bl 					=> 	NET_c3_p0_cmd_bl,
  cmd_byte_addr 			=> 	NET_c3_p0_cmd_byte_addr,
  wr_clk 					=> 	NET_c3_p0_wr_clk,
  wr_en 						=> 	NET_c3_p0_wr_en,
  wr_mask 					=> 	NET_c3_p0_wr_mask,
  wr_data 					=> 	NET_c3_p0_wr_data,
  rd_clk 					=> 	NET_c3_p0_rd_clk,
  rd_en 						=> 	NET_c3_p0_rd_en,
  rd_data 					=> 	NET_c3_p0_rd_data,
  rd_empty 					=> 	NET_c3_p0_rd_empty,
  cmd_full 					=> 	NET_c3_p0_cmd_full,
  wr_full 					=> 	NET_c3_p0_wr_full,
  wr_underrun 				=> 	NET_c3_p0_wr_underrun,
  wr_error 					=> 	NET_c3_p0_wr_error,
  rd_overflow 				=> 	NET_c3_p0_rd_overflow,
  rd_error 					=> 	NET_c3_p0_rd_error,
  rd_full 					=> 	NET_c3_p0_rd_full
);




end Behavioral;
