library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity LLSerialInterface is
    Port ( CLK100 : in  STD_LOGIC;
           RX : in  STD_LOGIC;
           DATA : out  STD_LOGIC_VECTOR (7 downto 0);
           DR : out  STD_LOGIC);
end LLSerialInterface;

architecture Behavioral of LLSerialInterface is

signal DATA_READY : STD_LOGIC := '0';
signal RXS : STD_LOGIC := '0';
signal RXS1 : STD_LOGIC := '0';
signal SERIAL_DATA: STD_LOGIC_VECTOR(9 downto 0) := (others => '0');
signal SERIAL_COUNTER : STD_LOGIC_VECTOR (4 downto 0) := (others => '0');

signal rxDelayLine: STD_LOGIC_VECTOR(5 downto 0);

begin

sanitize_input: process(CLK100)
begin
	if rising_edge(CLK100) then
		rxDelayLine(5) <= RX;
		rxDelayLine(4 downto 0) <= rxDelayLine(5 downto 1);
		RXS <= rxDelayLine(0);
	end if;
end process; 

receive_serial: process(CLK100)
begin
	if rising_edge(CLK100) then
		if (RXS = '1') AND (SERIAL_DATA(0) = '0') then
			SERIAL_DATA <=  "1111111111";
			DATA_READY <= '0';
			SERIAL_COUNTER <= "01011";
		else
			if (SERIAL_COUNTER = "10011") and (SERIAL_DATA(0) = '1')  then
				SERIAL_COUNTER <= (others => '0');
				SERIAL_DATA(8 downto 0) <= SERIAL_DATA(9 downto 1);
				SERIAL_DATA(9) <= NOT RXS;
				DATA_READY <= NOT SERIAL_DATA(1);
			else
				SERIAL_COUNTER <= SERIAL_COUNTER + 1;
				DATA_READY <= '0';
				SERIAL_DATA <= SERIAL_DATA;
			end if;
		end if;
	end if;
end process; 

DATA <= SERIAL_DATA(8 downto 1);
DR <= DATA_READY;

end Behavioral;

