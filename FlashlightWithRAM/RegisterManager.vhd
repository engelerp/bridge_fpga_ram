library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;


use work.SerialInterface.ALL;

entity RegisterManager is
    Port ( CLK100: in  STD_LOGIC;
           commandReceived: in  STD_LOGIC;
			  command: STD_LOGIC_VECTOR(5 downto 0);
           DATA: in  STD_LOGIC_VECTOR (17 downto 0);
			  Enabled: in STD_LOGIC;
           SerialData: out  SERIAL_DATA);
end RegisterManager;

architecture Behavioral of RegisterManager is

signal NX_SINGEN: STD_LOGIC_VECTOR(31 downto 0);
signal ADDR_SINE: STD_LOGIC_VECTOR(9 downto 0);
signal ADDR_BRAID: STD_LOGIC_VECTOR(14 downto 0);

begin

SerialData.Data <= DATA;
SerialData.Command <= command;
SerialData.CommandReceived <= commandReceived;
SerialData.NX_SINGEN <= NX_SINGEN;
SerialData.ADDR_SINE <= ADDR_SINE;
SerialData.ADDR_BRAID <= ADDR_BRAID;
SerialData.Enabled <= Enabled;

PROCESS_REGISTER_COMMAND: process(CLK100)
begin
	if rising_edge(CLK100) then
		--LD_PHASE_LOW
		if ((commandReceived = '1') AND (command = std_logic_vector(to_unsigned(22,6)))) then NX_SINGEN(17 downto 0) <= data; else NX_SINGEN(17 downto 0) <= NX_SINGEN(17 downto 0); end if;
		--LD_PHASE_HIGH
		if ((commandReceived = '1') AND (command = std_logic_vector(to_unsigned(23,6)))) then NX_SINGEN(31 downto 18) <= data(13	downto 0); else NX_SINGEN(23 downto 18) <= NX_SINGEN(23 downto 18); end if;
		--SET_ADDR_SINE
		if ((commandReceived = '1') AND (command = std_logic_vector(to_unsigned(20,6)))) then ADDR_SINE <= data(9 downto 0); else ADDR_SINE <= ADDR_SINE; end if;
		--BRAID_ADDRESS
		if ((commandReceived = '1') AND (command = std_logic_vector(to_unsigned(20,6)))) then ADDR_BRAID <= data(14 downto 0); else ADDR_BRAID <= ADDR_BRAID; end if;
	end if;
end process;

end Behavioral;

