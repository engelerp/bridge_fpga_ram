library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity SerialTransmit is
    Port ( CLK100 : in  STD_LOGIC;
           DR : in  STD_LOGIC;
           Data : in  STD_LOGIC_VECTOR (7 downto 0);
           TX : out  STD_LOGIC);
end SerialTransmit;

architecture Behavioral of SerialTransmit is

signal SERIAL_COUNTER: STD_LOGIC_VECTOR (4 downto 0) := (others => '0');
signal BUFF: STD_LOGIC_VECTOR(8 downto 0) := (others => '1');

begin

TX <= BUFF(0);

processTransmit: process(CLK100)
begin
	if rising_edge(CLK100) then
		if DR = '1' then
			BUFF(8 downto 1) <= Data;
			BUFF(0) <= '0';
			SERIAL_COUNTER <= (others => '0');
		else
			if SERIAL_COUNTER = "10011" then
				SERIAL_COUNTER <= (others => '0');
				BUFF(7 downto 0) <= BUFF(8 downto 1);
				BUFF(8) <= '1';
			else
				SERIAL_COUNTER <= SERIAL_COUNTER + 1;
				BUFF <= BUFF;
			end if;
		end if;
	end if;
end process;

end Behavioral;

