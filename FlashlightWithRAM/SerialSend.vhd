library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity SerialSend is
    Port ( CLK100 : in  STD_LOGIC;
           Data : in  STD_LOGIC_VECTOR (47 downto 0);
           Transfer : in  STD_LOGIC;
           TX : out  STD_LOGIC);
end SerialSend;

architecture Behavioral of SerialSend is

signal transferRegister: STD_LOGIC_VECTOR (71 downto 0) := (others => '0');
signal SERIAL_COUNTER : STD_LOGIC_VECTOR (4 downto 0) := (others => '0');

begin

TX <= transferRegister(0);

runTransfer: process(CLK100)
begin
	if rising_edge(CLK100) then
		if Transfer = '1' then
			SERIAL_COUNTER <= SERIAL_COUNTER;
			
			transferRegister(2 downto 0) <= (others => '1');
			transferRegister(3) <= '0';
			transferRegister(11 downto 4) <= Data(7 downto 0);
			
			transferRegister(14 downto 12) <= (others => '1');
			transferRegister(15) <= '0';
			transferRegister(23 downto 16) <= Data(15 downto 8);
			
			transferRegister(26 downto 24) <= (others => '1');
			transferRegister(27) <= '0';
			transferRegister(35 downto 28) <= Data(23 downto 16);
			
			transferRegister(38 downto 36) <= (others => '1');
			transferRegister(39) <= '0';
			transferRegister(47 downto 40) <= Data(31 downto 24);
			
			transferRegister(50 downto 48) <= (others => '1');
			transferRegister(51) <= '0';
			transferRegister(59 downto 52) <= Data(39 downto 32);
			
			transferRegister(62 downto 60) <= (others => '1');
			transferRegister(63) <= '0';
			transferRegister(71 downto 64) <= Data(47 downto 40);
			
		else
			if (SERIAL_COUNTER = "10011") then
				transferRegister(71) <= '1';
				transferRegister(70 downto 0) <= transferRegister(71 downto 1);
				SERIAL_COUNTER <= (others => '0');
			else
				SERIAL_COUNTER <= SERIAL_COUNTER + 1;
				transferRegister <= transferRegister;
			end if;
		end if;
	end if;
end process;


end Behavioral;

