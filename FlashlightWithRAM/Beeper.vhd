library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;


entity Beeper is
    Port ( CLK100 : in  STD_LOGIC;
           Beep : in  STD_LOGIC;
           Speakerout : out  STD_LOGIC;
			  Beeping: out STD_LOGIC);
end Beeper;

architecture Behavioral of Beeper is

signal enabled: STD_LOGIC := '0';
signal slowCounter: STD_LOGIC_VECTOR(23 downto 0) := (others => '0');
signal fastCounter: STD_LOGIC_VECTOR(13 downto 0) := (others => '0');
signal audio: STD_LOGIC := '0';

begin

Speakerout <=  audio;
Beeping <= enabled;

updateBeeper: process(CLK100)
begin
	if rising_edge(CLK100) then
		if ((Beep = '1') and (enabled = '0')) then
			enabled <= '1';
			slowCounter <= (others => '0');
			fastCounter <= (others => '0');
			audio <= '0';
		else
			if enabled = '1' then
				if fastCounter = 15000 then
					fastCounter <= (others => '0');
					audio <= NOT audio;
				else
					fastCounter <= fastCounter + 1;
					audio <= audio;
				end if;
				
				if slowCounter = 15000000 then
					slowCounter <= (others => '0');
					enabled <= '0';
				else
					slowCounter <= slowCounter + 1;
					enabled <= '1';
				end if;
			else
				slowCounter <= (others => '0');
				fastCounter <= (others => '0');
				enabled <= '0';
				audio <= '0';
			end if;
		end if;
	end if;
end process;


end Behavioral;

