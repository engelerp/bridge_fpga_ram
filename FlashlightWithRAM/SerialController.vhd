library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.SerialInterface.ALL;

entity SerialController is
    Port ( CLK100: in  STD_LOGIC;
           RX: in  STD_LOGIC;
			  BoardID: in STD_LOGIC_VECTOR(3 downto 0);
           SerialData: out  SERIAL_DATA);
end SerialController;

architecture Behavioral of SerialController is

--Inputs
signal currentByte : STD_LOGIC_VECTOR(7 downto 0);
signal dataReady : STD_LOGIC;

--Intermediate
signal boardActive: STD_LOGIC := '0';

--Outputs
signal cr : STD_LOGIC := '0';
signal dataRegister : STD_LOGIC_VECTOR(17 downto 0) := (others => '0');

component LLSerialInterface is
    Port ( CLK100 : in  STD_LOGIC;
           RX : in  STD_LOGIC;
           DATA : out  STD_LOGIC_VECTOR (7 downto 0);
           DR : out  STD_LOGIC);
end component;

component RegisterManager is
    Port ( CLK100: in  STD_LOGIC;
           commandReceived: in  STD_LOGIC;
			  command: STD_LOGIC_VECTOR(5 downto 0);
           DATA: in  STD_LOGIC_VECTOR (17 downto 0);
			  Enabled: in STD_LOGIC;
           SerialData: out  SERIAL_DATA);
end component;

begin

SERIALCORE: LLSerialInterface port map (CLK100, RX, currentByte, dataReady);
REGMAN: RegisterManager port map(CLK100, cr, currentByte(5 downto 0), dataRegister, boardActive, SerialData);

updateSM : process(CLK100)
begin
	if rising_edge(CLK100) then
		if dataReady = '1' then
			--Parse activation commands
			--SELECT_0 TO SELECT_14 AND SELECT_ALL (SELECT_15)
			if currentByte(7 downto 4) = "1110" then
				if currentByte(3 downto 0) = BoardID then 
					boardActive <= '1'; 
				else 
					boardActive <= '0'; 
				end if;
			else
				boardActive <= boardActive;
			end if;
			--Parse data commands
			if boardActive = '1' then
				if currentByte(7 downto 6) = "11" then cr <= '1'; else cr <= '0'; end if;
				if currentByte(7 downto 6) = "00" then dataRegister(5 downto 0) <= currentByte(5 downto 0); else dataRegister(5 downto 0) <= dataRegister(5 downto 0); end if;
				if currentByte(7 downto 6) = "01" then dataRegister(11 downto 6) <= currentByte(5 downto 0); else dataRegister(11 downto 6) <= dataRegister(11 downto 6); end if;
				if currentByte(7 downto 6) = "10" then dataRegister(17 downto 12) <= currentByte(5 downto 0); else dataRegister(17 downto 12) <= dataRegister(17 downto 12); end if;
			else
				dataRegister <= dataRegister;
				cr <= '0';
			end if;
		else 
			boardActive <= boardActive;
			dataRegister <= dataRegister;
			cr <= '0';
			dataRegister <= dataRegister;
		end if;
	end if;
end process;
end Behavioral;

