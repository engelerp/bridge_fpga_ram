library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Router is
    Port ( 	--CLK100: in STD_LOGIC; --replaced by c3_sys_clk
				RX_IN : in  STD_LOGIC;
				OPT_OUT : out  STD_LOGIC_VECTOR(9 downto 0);
				LEDS: out STD_LOGIC_VECTOR(2 downto 0);
				BEEPER_OUT: out STD_LOGIC;
				LASER_CLK_1_P: in STD_LOGIC;
				LASER_CLK_1_N: in STD_LOGIC;
				LASER_SIGNAL_1_P: in STD_LOGIC;
				LASER_SIGNAL_1_N: in STD_LOGIC;
				LASER_SIGNAL_2_P: in STD_LOGIC;
				LASER_SIGNAL_2_N: in STD_LOGIC;
				LASER_SIGNAL_3_P: in STD_LOGIC;
				LASER_SIGNAL_3_N: in STD_LOGIC;
				LASER_CLK_2_P: in STD_LOGIC;
				LASER_CLK_2_N: in STD_LOGIC;
				LASER_CLK_3_P: in STD_LOGIC;
				LASER_CLK_3_N: in STD_LOGIC;
				STX: out STD_LOGIC;
        mcb3_dram_dq : inout  STD_LOGIC_VECTOR (15 downto 0); --LPDDR pin
        mcb3_dram_a : out  STD_LOGIC_VECTOR (12 downto 0); --LPDDR pin
        mcb3_dram_ba : out  STD_LOGIC_VECTOR (1 downto 0); --LPDDR pin
        mcb3_dram_ras_n : out  STD_LOGIC; --LPDDR pin
        mcb3_dram_cas_n : out  STD_LOGIC; --LPDDR pin
        mcb3_dram_we_n : out  STD_LOGIC; --LPDDR pin
        mcb3_dram_cke : out  STD_LOGIC; --LPDDR pin
        mcb3_dram_ck : out  STD_LOGIC; --LPDDR pin
        mcb3_dram_ck_n : out  STD_LOGIC; --LPDDR pin
        mcb3_dram_dqs : inout  STD_LOGIC; --LPDDR pin
        mcb3_dram_udqs : inout  STD_LOGIC; --LPDDR pin
        mcb3_dram_udm : out  STD_LOGIC; --LPDDR pin
        mcb3_dram_dm : out  STD_LOGIC; --LPDDR pin
        mcb3_rzq : inout  STD_LOGIC; --LPDDR pin
        c3_sys_clk : in STD_LOGIC; --100 MHz clock input
        c3_sys_rst_n : in  STD_LOGIC);
end Router;

architecture Behavioral of Router is

--new clock signal net
signal CLK100: STD_LOGIC;

--carli signals
signal c_error: STD_LOGIC;

signal clockTick: STD_LOGIC;
signal DR_Received: STD_LOGIC;
signal DataReceived: STD_LOGIC_VECTOR(7 downto 0);
signal DR_Transmit: STD_LOGIC;
signal DataToTransmit: STD_LOGIC_VECTOR(7 downto 0);
signal TX: STD_LOGIC;
signal nottedIN: STD_LOGIC;
signal clockReset: STD_LOGIC := '0';
signal beepingNow: STD_LOGIC;


signal beepOn: STD_LOGIC;
signal beepOut: STD_LOGIC;

signal clockEnabled: STD_LOGIC := '0';

signal laserClock1: STD_LOGIC;
signal laserClock2: STD_LOGIC;
signal laserClock3: STD_LOGIC;

signal laserClock2N: STD_LOGIC;

signal laserData1: STD_LOGIC;
signal laserData2: STD_LOGIC;
signal laserData3: STD_LOGIC;

signal laserData1N: STD_LOGIC;

signal requestTransfer: STD_LOGIC := '0';

signal enabledClockTick: STD_LOGIC := '0';
signal CTS: STD_LOGIC; -- Inhibits beep

component SerialTransmit is
    Port ( CLK100 : in  STD_LOGIC;
           DR : in  STD_LOGIC;
           Data : in  STD_LOGIC_VECTOR (7 downto 0);
           TX : out  STD_LOGIC);
end component;

component LLSerialInterfaceR is
    Port ( CLK100 : in  STD_LOGIC;
           RX : in  STD_LOGIC;
           DATA : out  STD_LOGIC_VECTOR (7 downto 0);
           DR : out  STD_LOGIC);
end component;

component ClockGenerator is
    Port ( CLK100 : in  STD_LOGIC;
			  Reset: in STD_LOGIC;
           Tick : out  STD_LOGIC);
end component;

component Beeper is
    Port ( CLK100 : in  STD_LOGIC;
           Beep : in  STD_LOGIC;
           Speakerout : out  STD_LOGIC;
			  Beeping: out STD_LOGIC);
end component;

component LaserInterface is
    Port ( CLK100 : in  STD_LOGIC;
           LS1 : in  STD_LOGIC;
           LC1 : in  STD_LOGIC;
           LS2 : in  STD_LOGIC;
           LC2 : in  STD_LOGIC;
           LS3 : in  STD_LOGIC;
           LC3 : in  STD_LOGIC;
           sampleClock : in  STD_LOGIC;
			     RX_SR: in STD_LOGIC;
           TX : out  STD_LOGIC;
			     CTS: out STD_LOGIC;

           CLKPLL100 : out STD_LOGIC; --pll output, to be used in rest of design
           ERROR : out STD_LOGIC; --error signal
           mcb3_dram_dq : inout  STD_LOGIC_VECTOR (15 downto 0); --LPDDR pin
           mcb3_dram_a : out  STD_LOGIC_VECTOR (12 downto 0); --LPDDR pin
           mcb3_dram_ba : out  STD_LOGIC_VECTOR (1 downto 0); --LPDDR pin
           mcb3_dram_ras_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_cas_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_we_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_cke : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_ck : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_ck_n : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_dqs : inout  STD_LOGIC; --LPDDR pin
           mcb3_dram_udqs : inout  STD_LOGIC; --LPDDR pin
           mcb3_dram_udm : out  STD_LOGIC; --LPDDR pin
           mcb3_dram_dm : out  STD_LOGIC; --LPDDR pin
           mcb3_rzq : inout  STD_LOGIC; --LPDDR pin
           c3_sys_rst_n : in  STD_LOGIC););
end component;

begin

iBeeper: Beeper port map(CLK100, beepOn, beepOut, beepingNow);
iClockGenerator: ClockGenerator port map(CLK100, clockReset, clockTick);
iLLSerialInterface: LLSerialInterfaceR port map(CLK100, nottedIN, DataReceived, DR_Received);
iSerialTransmit: SerialTransmit port map(CLK100, DR_Transmit, DataToTransmit, TX);
--iLaserInterface: LaserInterface port map(CLK100, laserData1, laserClock1, laserData2, laserClock2, laserData3, laserClock3, enabledClockTick, nottedIN, STX, CTS);
iLaserInterface : LaserInterfaceWithCarli port map(
          c3_sys_clk => c3_sys_clk,
          LS1 => laserData1,
          LC1 => laserClock1,
          LS2 => laserData2,
          LC2 => laserClock2,
          LS3 => laserData3,
          LC3 => laserClock3,
          sampleClock => enabledClockTick,
          RX_SR => nottedIN,
          TX => STX,
          CTS => CTS,
          CLKPLL100 => CLK100,
          ERROR => c_error,
          mcb3_dram_dq => mcb3_dram_dq,
          mcb3_dram_a => mcb3_dram_a,
          mcb3_dram_ba => mcb3_dram_ba,
          mcb3_dram_ras_n => mcb3_dram_ras_n,
          mcb3_dram_cas_n => mcb3_dram_cas_n,
          mcb3_dram_we_n => mcb3_dram_we_n,
          mcb3_dram_cke => mcb3_dram_cke,
          mcb3_dram_ck => mcb3_dram_ck,
          mcb3_dram_ck_n => mcb3_dram_ck_n,
          mcb3_dram_dqs => mcb3_dram_dqs,
          mcb3_dram_udqs => mcb3_dram_udqs,
          mcb3_dram_udm => mcb3_dram_udm,
          mcb3_dram_dm => mcb3_dram_dm,
          mcb3_rzq => mcb3_rzq,
          c3_sys_rst_n => c3_sys_rst_n);


i_IBUFDS1 : IBUFDS generic map(IOSTANDARD => "DEFAULT", DIFF_TERM => TRUE) port map(laserClock1, LASER_CLK_1_P, LASER_CLK_1_N);
i_IBUFDS2 : IBUFDS generic map(IOSTANDARD => "DEFAULT", DIFF_TERM => TRUE) port map(laserClock2N, LASER_CLK_2_P, LASER_CLK_2_N);
i_IBUFDS3 : IBUFDS generic map(IOSTANDARD => "DEFAULT", DIFF_TERM => TRUE) port map(laserClock3, LASER_CLK_3_P, LASER_CLK_3_N);
i_IBUFDS4 : IBUFDS generic map(IOSTANDARD => "DEFAULT", DIFF_TERM => TRUE) port map(laserData1N, LASER_SIGNAL_1_P, LASER_SIGNAL_1_N);
i_IBUFDS5 : IBUFDS generic map(IOSTANDARD => "DEFAULT", DIFF_TERM => TRUE) port map(laserData2, LASER_SIGNAL_2_P, LASER_SIGNAL_2_N);
i_IBUFDS6 : IBUFDS generic map(IOSTANDARD => "DEFAULT", DIFF_TERM => TRUE) port map(laserData3, LASER_SIGNAL_3_P, LASER_SIGNAL_3_N);

laserClock2 <= NOT laserClock2N;
laserData1 <= NOT laserData1N;


--CLK_OUT <= laserSignal1;
nottedIN <= NOT RX_IN;
OPT_OUT <= (others => TX);
BEEPER_OUT <= beepOut;

LEDS(0) <= '1';

LEDS(1) <= clockEnabled;
LEDS(2) <= beepingNow;

updateCGEN: process(CLK100)
begin
	if rising_edge(CLK100) then
		if ((DR_Received = '1') AND (DataReceived = std_logic_vector(to_unsigned(218,8)))) then
			requestTransfer <= '0';
			clockEnabled <= '1';
			clockReset <= '1';
			beepOn <= '0';
		elsif ((DR_Received = '1') AND (DataReceived = std_logic_vector(to_unsigned(219,8)))) then
			requestTransfer <= '0';
			clockEnabled <= '0';
			clockReset <= '1';
			beepOn <= '0';
		else
			requestTransfer <= '0';
			beepOn <= (DR_Received AND clockEnabled) AND (NOT CTS);
			clockEnabled <= clockEnabled;
			clockReset <= '0';
		end if;
	end if;
end process;

updateENCT: process(CLK100)
begin
	if rising_edge(CLK100) then
		enabledClockTick <= (clockEnabled AND clockTick);
	end if;
end process;

updateRecv: process(CLK100)
begin
	if rising_edge(CLK100) then
		if clockEnabled = '1' then
			if clockTick = '1' then
				DataToTransmit <= (others => '1');
				DR_Transmit <= '1';
			else
				DataToTransmit <= (others => '0');
				DR_Transmit <= '0';
			end if;
		else
			if DR_Received = '1' then
				DataToTransmit <= DataReceived;
				DR_Transmit <= '1';
			else
				DataToTransmit <= (others => '0');
				DR_Transmit <= '0';
			end if;
		end if;
	end if;
end process;

end Behavioral;
